/*
JavaScript Snake
First version by Patrick Gillespie - I've since merged in a good number of github pull requests
http://patorjk.com/games/snake
*/

/**
* @module Snake
* @class SNAKE
*/

//'use strict';
var MAX_BOARD_COLS = document.getElementById('width').value,
    MAX_BOARD_ROWS = document.getElementById('height').value;
var SNAKE = SNAKE || {};

/**
* @method addEventListener
* @param {Object} obj The object to add an event listener to.
* @param {String} event The event to listen for.
* @param {Function} funct The function to execute when the event is triggered.
* @param {Boolean} evtCapturing True to do event capturing, false to do event bubbling.
*/
SNAKE.addEventListener = (function() {
    if (window.addEventListener) {
        return function(obj, event, funct, evtCapturing) {
            obj.addEventListener(event, funct, evtCapturing);
        };
    } else if (window.attachEvent) {
        return function(obj, event, funct) {
            obj.attachEvent("on" + event, funct);
        };
    }
})();

/**
* @method removeEventListener
* @param {Object} obj The object to remove an event listener from.
* @param {String} event The event that was listened for.
* @param {Function} funct The function that was executed when the event is triggered.
* @param {Boolean} evtCapturing True if event capturing was done, false otherwise.
*/

SNAKE.removeEventListener = (function() {
    if (window.removeEventListener) {
        return function(obj, event, funct, evtCapturing) {
            obj.removeEventListener(event, funct, evtCapturing);
        };
    } else if (window.detachEvent) {
        return function(obj, event, funct) {
            obj.detachEvent("on" + event, funct);
        };
    }
})();

/**
* This class manages the snake which will reside inside of a SNAKE.Board object.
* @class Snake
* @constructor
* @namespace SNAKE
* @param {Object} config The configuration object for the class. Contains playingBoard (the SNAKE.Board that this snake resides in), startRow and startCol.
*/
SNAKE.Snake = SNAKE.Snake || (function() {

    // -------------------------------------------------------------------------
    // Private static variables and methods
    // -------------------------------------------------------------------------

    var instanceNumber = 0;
    var blockPool = [];

    var SnakeBlock = function() {
        this.elm = null;
        this.elmStyle = null;
        this.row = -1;
        this.col = -1;
        this.xPos = -1000;
        this.yPos = -1000;
        this.next = null;
        this.prev = null;
    };

    // this function is adapted from the example at http://greengeckodesign.com/blog/2007/07/get-highest-z-index-in-javascript.html
    function getNextHighestZIndex(myObj) {
        var highestIndex = 0,
            currentIndex = 0,
            ii;
        for (ii in myObj) {
            if (myObj[ii].elm.currentStyle){
                currentIndex = parseFloat(myObj[ii].elm.style["z-index"],10);
            }else if(window.getComputedStyle) {
                currentIndex = parseFloat(document.defaultView.getComputedStyle(myObj[ii].elm,null).getPropertyValue("z-index"),10);
            }
            if(!isNaN(currentIndex) && currentIndex > highestIndex){
                highestIndex = currentIndex;
            }
        }
        return(highestIndex+1);
    }

    // -------------------------------------------------------------------------
    // Contructor + public and private definitions
    // -------------------------------------------------------------------------

    /*
        config options:
            playingBoard - the SnakeBoard that this snake belongs too.
            startRow - The row the snake should start on.
            startCol - The column the snake should start on.
    */
    return function(config) {

        if (!config||!config.playingBoard) {return;}

        // ----- private variables -----

        var me = this,
            safeMoves = 50,
            shouldTurn = true,
            playingBoard = config.playingBoard,
            myId = instanceNumber++,
            growthIncr = 5,
            moveQueue = [], // a queue that holds the next moves of the snake
            currentDirection = 1, // 0: up, 1: left, 2: down, 3: right
            columnShift = [0, 1, 0, -1],
            rowShift = [-1, 0, 1, 0],
            xPosShift = [],
            yPosShift = [],
            snakeSpeed = 75,
            jumpiness = 1,
            isDead = false,
            isPaused = false;

            function setModeListener (mode, speed) {
                document.getElementById(mode).addEventListener('click', function () { snakeSpeed = speed; });
            }

            var modeDropdown = document.getElementById('selectMode');
            if ( modeDropdown ) {
                modeDropdown.addEventListener('change', function(evt) {
                    evt = evt || {};
                    var val = evt.target ? parseInt(evt.target.value) : 75;
                    
                    if (isNaN(val)) {
                        val = 75;
                    }

                    snakeSpeed = val;
                    if(snakeSpeed < 0){
                        document.getElementById('jumpinessChooser').style.display = "none";
                        jumpiness = 1;
                    }
                    else{
                        document.getElementById('jumpinessChooser').style.display = "inline";
                        jumpiness = 1;
                    }
                    
                    document.getElementById("modeDisplay").innerHTML = document.getElementById("selectMode").options[document.getElementById("selectMode").selectedIndex].text;

                    setTimeout(function() {
                        document.getElementById('game-area').focus();
                    }, 10);
                });
            }
            var jumpDropdown = document.getElementById('selectJump');
            if ( jumpDropdown ) {
                jumpDropdown.addEventListener('change', function(evt) {
                    evt = evt || {};
                    var val = evt.target ? parseInt(evt.target.value) : 1;
                    
                    if (isNaN(val)) {
                        val = 1;
                    }

                    jumpiness = val;
                    
                    document.getElementById("jumpDisplay").innerHTML = document.getElementById("selectJump").options[document.getElementById("selectJump").selectedIndex].text;

                    setTimeout(function() {
                        document.getElementById('game-area').focus();
                    }, 10);
                });
            }
            var invincible;
            var wraparound;
            var invincibleBox = document.getElementById('toggleInvincibility');
            var wraparoundBox = document.getElementById('toggleWraparound');

            invincibleBox.addEventListener('change', function(evt){invincible = this.checked;});
            wraparoundBox.addEventListener('change', function(evt){wraparound = this.checked;});
            var wInput = document.getElementById('width');
            var hInput = document.getElementById('height');

            wInput.addEventListener('change', function(evt){MAX_BOARD_COLS = this.value});
            hInput.addEventListener('change', function(evt){MAX_BOARD_ROWS = this.value});
            //setModeListener('Easy', 100);
            //setModeListener('Medium', 75);
            //setModeListener('Difficult', 50);
            //setModeListener('Very Difficult', 25);

        // ----- public variables -----
        me.snakeBody = {};
        me.snakeBody["b0"] = new SnakeBlock(); // create snake head
        me.snakeBody["b0"].row = config.startRow || 1;
        me.snakeBody["b0"].col = config.startCol || 1;
        me.snakeBody["b0"].xPos = me.snakeBody["b0"].row * playingBoard.getBlockWidth();
        me.snakeBody["b0"].yPos = me.snakeBody["b0"].col * playingBoard.getBlockHeight();
        me.snakeBody["b0"].elm = createSnakeElement();
        me.snakeBody["b0"].elmStyle = me.snakeBody["b0"].elm.style;
        playingBoard.getBoardContainer().appendChild( me.snakeBody["b0"].elm );
        me.snakeBody["b0"].elm.style.left = me.snakeBody["b0"].xPos + "px";
        me.snakeBody["b0"].elm.style.top = me.snakeBody["b0"].yPos + "px";
        me.snakeBody["b0"].next = me.snakeBody["b0"];
        me.snakeBody["b0"].prev = me.snakeBody["b0"];

        me.snakeLength = 1;
        me.moves = 0;
        me.snakeHead = me.snakeBody["b0"];
        me.snakeTail = me.snakeBody["b0"];
        me.snakeHead.elm.className = me.snakeHead.elm.className.replace(/\bsnake-snakebody-dead\b/,'');
        me.snakeHead.elm.className += " snake-snakebody-alive";

        // ----- private methods -----

        function createSnakeElement() {
            var tempNode = document.createElement("div");
            tempNode.className = "snake-snakebody-block";
            tempNode.style.left = "-1000px";
            tempNode.style.top = "-1000px";
            tempNode.style.width = playingBoard.getBlockWidth() + "px";
            tempNode.style.height = playingBoard.getBlockHeight() + "px";
            return tempNode;
        }

        function createBlocks(num) {
            var tempBlock;
            var tempNode = createSnakeElement();

            for (var ii = 1; ii < num; ii++){
                tempBlock = new SnakeBlock();
                tempBlock.elm = tempNode.cloneNode(true);
                tempBlock.elmStyle = tempBlock.elm.style;
                playingBoard.getBoardContainer().appendChild( tempBlock.elm );
                blockPool[blockPool.length] = tempBlock;
            }

            tempBlock = new SnakeBlock();
            tempBlock.elm = tempNode;
            playingBoard.getBoardContainer().appendChild( tempBlock.elm );
            blockPool[blockPool.length] = tempBlock;
            
            console.log(tempNode);
        }

        // ----- public methods -----

        me.setPaused = function(val) {
            isPaused = val;
        };
        me.getPaused = function() {
            return isPaused;
        };

        /**
        * This method is called when a user presses a key. It logs arrow key presses in "moveQueue", which is used when the snake needs to make its next move.
        * @method handleArrowKeys
        * @param {Number} keyNum A number representing the key that was pressed.
        */
        /*
            Handles what happens when an arrow key is pressed.
            Direction explained (0 = up, etc etc)
                    0
                  3   1
                    2
        */
        me.handleArrowKeys = function(keyNum) {
            if (isDead || isPaused || document.getElementById('selectAI').value > 0) {return;}

            var snakeLength = me.snakeLength;
            var lastMove = moveQueue[0] || currentDirection;

            //console.log(moveQueue);
            //console.log("currentdir="+currentDirection);
            //console.log("lastmove="+lastMove);
            //console.log("dir="+keyNum);
            //console.log(document.getElementById('selectAI').value);

            switch (keyNum) {
                case 37:
                case 65:
                    if ( lastMove !== 1 || snakeLength === 1 && me.snakeHead.row == config.startRow && me.snakeHead.col == config.startCol ) {
                        moveQueue.unshift(3); //SnakeDirection = 3;
                    }
                    break;
                case 38:
                case 87:
                    if ( lastMove !== 2 || snakeLength === 1 && me.snakeHead.row == config.startRow && me.snakeHead.col == config.startCol ) {
                        moveQueue.unshift(0);//SnakeDirection = 0;
                    }
                    break;
                case 39:
                case 68:
                    if ( lastMove !== 3 || snakeLength === 1 && me.snakeHead.row == config.startRow && me.snakeHead.col == config.startCol ) {
                        moveQueue.unshift(1); //SnakeDirection = 1;
                    }
                    break;
                case 40:
                case 83:
                    if ( lastMove !== 0 || snakeLength === 1 && me.snakeHead.row == config.startRow && me.snakeHead.col == config.startCol ) {
                        moveQueue.unshift(2);//SnakeDirection = 2;
                    }
                    break;
            }
        };
        
        //This method determines whether the snake's attempted next move will be safe or not.
        me.evalSafety = function(nextDir) {
            var curHead = me.snakeHead,
            grid = playingBoard.grid, // cache grid for quicker lookup
            trueCol = curHead.col+columnShift[nextDir],
            trueRow = curHead.row+rowShift[nextDir];
            if(wraparound){
                if(trueRow <= 0){
                trueRow = grid.length-2;
                }
                if(trueRow >= grid.length-1){
                    trueRow = 1;
                }
                if(trueCol == 0){
                    trueCol = grid[trueRow].length-2;
                }
                if(trueCol == grid[trueRow].length-1){
                    trueCol = 1;
                }
            }
            else{
                if(trueRow <= 0){
                    return 0;
                }
                if(trueRow >= grid.length-1){
                    return 0;
                }
                if(trueCol == 0){
                    return 0;
                }
                if(trueCol == grid[trueRow].length-1){
                    return 0;
                }
            }
            if ( grid[trueRow][trueCol] > 0 ) {return 0;}
            else {return 1;}
            
        }
        me.DevalSafety = function(nextDir) {
            var curHead = me.snakeHead,
            grid = playingBoard.grid; // cache grid for quicker lookup
            if ( grid[curHead.col + columnShift[nextDir]][curHead.row + rowShift[nextDir]] > 0 ) {return 0;}
            else {return 1;}

        }
        me.findSafeMoves = function(newDir) {
            console.log(newDir);
            var curHead = me.snakeHead,
                grid = playingBoard.grid;
            var maxRows = grid.length-1;
            var maxCols = grid[0].length-1;
            if ( newDir === 0 ) {
                for (i = curHead.row-1; i >= 0; i--) {
                    if ( grid[i][curHead.col] > 0 ) {console.log("neewDir0"); console.log(curHead.row-i-1); return curHead.row-i-1;}
                    if ( grid[i][curHead.col] < 0 ) {shouldTurn=false; return 500;}
                }
            } else if ( newDir === 2 ) {
                for (i = curHead.row+1; i <= maxRows+1; i++) {
                    if ( grid[i][curHead.col] > 0 ) {console.log("neewDir2"); console.log(i-curHead.row-1); return i-curHead.row-1;}
                    if ( grid[i][curHead.col] > 0 ) {shouldTurn=false; return 500;}
                }
            } else if ( newDir === 1 ) {
                for (i = curHead.col+1; i <= maxCols+1; i++) {
                    if ( grid[curHead.row][i] > 0 ) {console.log("neewDir1"); console.log(i-curHead.col-1); return i-curHead.col-1;}
                    if ( grid[curHead.row][i] > 0 ) {shouldTurn=false; return 500;}
                }
            } else if ( newDir === 3 ) {
                for (i = curHead.col-1; i >= 0; i--) {
                    if ( grid[curHead.row][i] > 0 ) {console.log("neewDir3"); console.log(curHead.col-i-1); return curHead.col-i-1;}
                    if ( grid[curHead.row][i] > 0 ) {shouldTurn=false; return 500;}
                }
            }
        }

        //This method evaluates the next move for the snake if AI is enabled.
        me.evalAI = function() {
            
            if (document.getElementById('selectAI').value === 0) {return;}
            if (document.getElementById('selectAI').value == 1){
                var curHead = me.snakeHead;
            
                randChoice = Math.floor(Math.random() * 100);
                console.log("randChoice= "+randChoice)
                
                if (me.snakeHead.row == config.startRow && me.snakeHead.col == config.startCol) {
                    moveQueue.unshift(1);
                }
                
                safeMoves--;
                
                if (safeMoves === 0 ) {
                    console.log("bad bad bad");
                    if ( ((currentDirection === 1 || currentDirection === 3) && me.evalSafety(2)) || ((currentDirection === 0 || currentDirection === 2) && me.DevalSafety(3)) ) {
                        randChoice = 1;
                    } else { randChoice = 6; }
                }
                
                console.log(safeMoves+"safe");
                
                //if (shouldTurn) {
                //    if (curHead.row === myFood.row && curHead.col > myFood.col && me.evalSafety(3)) { moveQueue.unshift(3); safeMoves = me.findSafeMoves(3); }
                //    if (curHead.row === myFood.row && curHead.col < myFood.col && me.evalSafety(1)) { moveQueue.unshift(1); safeMoves = me.findSafeMoves(1); }
                //    if (curHead.col === myFood.col && curHead.row > myFood.row && me.evalSafety(0)) { moveQueue.unshift(0); safeMoves = me.findSafeMoves(0); }
                //    if (curHead.col === myFood.col && curHead.row > myFood.row && me.evalSafety(2)) { moveQueue.unshift(2); safeMoves = me.findSafeMoves(2); }
                //}
                
                if (shouldTurn) {
                    if ( randChoice < 10 ) {
                        if ( currentDirection === 1 || currentDirection === 3 ) {
                            if ( randChoice > 5 ) {
                                if ( me.DevalSafety(0) ) { moveQueue.unshift(0); safeMoves = me.findSafeMoves(0); }
                                return;
                            } else {
                                if ( me.DevalSafety(2) ) { moveQueue.unshift(2); safeMoves = me.findSafeMoves(2); }
                                return;
                            }
                        }
                        else if ( currentDirection === 0 || currentDirection === 2 ) {
                            if ( randChoice > 5 ) {
                                if ( me.DevalSafety(1) ) { moveQueue.unshift(1); safeMoves = me.findSafeMoves(1); }
                                return;
                            } else {
                                if ( me.DevalSafety(3) ) { moveQueue.unshift(3); safeMoves = me.findSafeMoves(3); }
                                return;
                            }
                        }
                    }
                    else {return;}
                }



            }
            if(document.getElementById('selectAI').value == 2){
                randChoice = Math.floor(Math.random() * 100);
                var c;
                var grid = playingBoard.grid;
                for(var i = 0; i < grid.length; i++){
                    for(var j = 0; j < grid[i].length; j++){
                        
                        if(grid[i][j] === playingBoard.getGridFoodValue()){
                            c = j;
                        }
                        
                    }
                }
                if(me.snakeHead.col === c && me.evalSafety(2)){
                    moveQueue.unshift(2);
                }
                else if(me.evalSafety(currentDirection) && (currentDirection == 1 || currentDirection == 3)){
                    moveQueue.unshift(currentDirection);
                }
                else if(me.evalSafety((randChoice % 50) > 25 ? 3 : (randChoice > 50 ? 1 : 0)) == 1){
                    moveQueue.unshift((randChoice % 50) > 25 ? 3 : (randChoice > 50 ? 1 : 0));
                }
                else if(me.evalSafety((randChoice % 50) > 25 ? 1 : (randChoice > 50 ? 0 : 3)) == 1){
                    moveQueue.unshift((randChoice % 50) > 25 ? 1 : (randChoice > 50 ? 0 : 3));
                }
                else{
                    moveQueue.unshift((randChoice % 50) > 25 ? (randChoice > 50 ? 0 : 3) : 1);
                }
            }
        }
        /**
        * This method is executed for each move of the snake. It determines where the snake will go and what will happen to it. This method needs to run quickly.
        * @method go
        */
        
        me.go = function() {

            var oldHead,
                newHead,
                myDirection,
                dead = false,
                grid; // cache grid for quicker lookup
            
            for(var i = 0; i < jumpiness; i++){
                oldHead = me.snakeHead,
                newHead = me.snakeTail,
                myDirection = currentDirection,
                grid = playingBoard.grid; // cache grid for quicker lookup

                if (isPaused === true) {
                    setTimeout(function(){me.go();}, snakeSpeed);
                    return;
                }
                
                me.moves++;
                //console.log(me.moves);
                //playingBoard.elmMovePanel.innerHTML = "Moves: " + me.moves;
                
                if (document.getElementById('selectAI').value > 0) {
                    me.evalAI();
                }

                me.snakeTail = newHead.prev;
                me.snakeHead = newHead;

                // clear the old board position
                if ( grid[newHead.row] && grid[newHead.row][newHead.col] ) {
                    grid[newHead.row][newHead.col] = 0;
                }

                if (moveQueue.length){
                    myDirection = currentDirection = moveQueue.pop();
                }

                newHead.col = oldHead.col + columnShift[myDirection];
                newHead.row = oldHead.row + rowShift[myDirection];
                newHead.xPos = oldHead.xPos + xPosShift[myDirection];
                newHead.yPos = oldHead.yPos + yPosShift[myDirection];
                if(wraparound){
                    if(newHead.col == 0){
                        newHead.col = grid[newHead.row].length-2;
                        newHead.xPos = (grid[newHead.row].length-2)*xPosShift[myDirection]*-1;
                    }
                    if(newHead.col == grid[newHead.row].length-1){
                        newHead.col = 1;
                        newHead.xPos = xPosShift[myDirection];
                    }
                    if(newHead.row == 0){
                        newHead.row = grid.length-2;
                        newHead.yPos = (grid.length-2)*yPosShift[myDirection]*-1;
                    }
                    if(newHead.row == grid.length-1){
                        newHead.row = 1;
                        newHead.yPos = yPosShift[myDirection];
                    }
                }
                console.log(newHead.col);
                if ( !newHead.elmStyle ) {
                    newHead.elmStyle = newHead.elm.style;
                }

                newHead.elmStyle.left = newHead.xPos + "px";
                newHead.elmStyle.top = newHead.yPos + "px";
                playingBoard.moveMade();
                if (grid[newHead.row][newHead.col] === 0) {
                    grid[newHead.row][newHead.col] = 1;
                } else if (grid[newHead.row][newHead.col] > 0 && !invincible) {
                    me.handleDeath();
                    dead = true;
                } else if (grid[newHead.row][newHead.col] === playingBoard.getGridFoodValue()) {
                    grid[newHead.row][newHead.col] = 1;
                    me.eatFood();
                }
            }
            if(snakeSpeed < 0){
                var iMax;
                if(snakeSpeed == -1){
                    iMax = 2;
                }
                if(snakeSpeed == -2){
                    iMax = 3;
                }
                if(snakeSpeed == -3){
                    iMax = 5;
                }
                if(snakeSpeed == -4){
                    iMax = 10;
                }
                if(snakeSpeed == -5){
                    iMax = 25;
                }
                if(snakeSpeed == -6){
                    iMax = 50;
                }
                if(snakeSpeed == -7){
                    iMax = 100;
                }
                if(snakeSpeed == -8){
                    iMax = 250;
                }
                if(snakeSpeed == -9){
                    iMax = 3000;
                }
                if(snakeSpeed == -10){
                    iMax = 50000;
                }
                if(snakeSpeed == -11){
                    iMax = 2500000
                }
                if(snakeSpeed == -12){
                    iMax = 800000000
                }
                if(snakeSpeed == -13){
                    iMax = 150000000000
                }
                for(var i = 0; i < iMax; i++){
                    oldHead = me.snakeHead;
                    newHead = me.snakeTail;
                    myDirection = currentDirection;
                    grid = playingBoard.grid; // cache grid for quicker lookup

                    if (isPaused === true) {
                        setTimeout(function(){me.go();}, snakeSpeed);
                        return;
                    }

                    me.moves++;
                    //console.log(me.moves);
                    //playingBoard.elmMovePanel.innerHTML = "Moves: " + me.moves;

                    if (document.getElementById('selectAI').value > 0) {
                        me.evalAI();
                    }

                    me.snakeTail = newHead.prev;
                    me.snakeHead = newHead;

                    // clear the old board position
                    if ( grid[newHead.row] && grid[newHead.row][newHead.col] ) {
                        grid[newHead.row][newHead.col] = 0;
                    }

                    if (moveQueue.length){
                        myDirection = currentDirection = moveQueue.pop();
                    }

                    newHead.col = oldHead.col + columnShift[myDirection];
                    newHead.row = oldHead.row + rowShift[myDirection];
                    newHead.xPos = oldHead.xPos + xPosShift[myDirection];
                    newHead.yPos = oldHead.yPos + yPosShift[myDirection];
                    if(wraparound){
                        if(newHead.col == 0){
                            newHead.col = grid[newHead.row].length-2;
                            newHead.xPos = (grid[newHead.row].length-2)*xPosShift[myDirection]*-1;
                        }
                        if(newHead.col == grid[newHead.row].length-1){
                            newHead.col = 1;
                            newHead.xPos = xPosShift[myDirection];
                        }
                        if(newHead.row == 0){
                            newHead.row = grid.length-2;
                            newHead.yPos = (grid.length-2)*yPosShift[myDirection]*-1;
                        }
                        if(newHead.row == grid.length-1){
                            newHead.row = 1;
                            newHead.yPos = yPosShift[myDirection];
                        }
                    }
                    if ( !newHead.elmStyle ) {
                        newHead.elmStyle = newHead.elm.style;
                    }

                    newHead.elmStyle.left = newHead.xPos + "px";
                    newHead.elmStyle.top = newHead.yPos + "px";
                    playingBoard.moveMade();
                    if (grid[newHead.row][newHead.col] === 0) {
                        grid[newHead.row][newHead.col] = 1;
                    } else if (grid[newHead.row][newHead.col] > 0 && !invincible) {
                        me.handleDeath();
                        dead = true;
                    } else if (grid[newHead.row][newHead.col] === playingBoard.getGridFoodValue()) {
                        grid[newHead.row][newHead.col] = 1;
                        me.eatFood();
                    }
                }
            }
            // check the new spot the snake moved into
            
            if(!dead){
                setTimeout(function(){me.go();}, snakeSpeed);
            }
        };

        /**
        * This method is called when it is determined that the snake has eaten some food.
        * @method eatFood
        */
        me.eatFood = function() {
            
            if (blockPool.length <= growthIncr) {
                createBlocks(growthIncr*2);
            }
            var blocks = blockPool.splice(0, growthIncr);
            
            //console.log(blocks);

            var ii = blocks.length,
                index,
                prevNode = me.snakeTail;
            while (ii--) {
                index = "b" + me.snakeLength++;
                me.snakeBody[index] = blocks[ii];
                me.snakeBody[index].prev = prevNode;
                me.snakeBody[index].elm.className = me.snakeHead.elm.className.replace(/\bsnake-snakebody-dead\b/,'');
                me.snakeBody[index].elm.className += " snake-snakebody-alive";
                prevNode.next = me.snakeBody[index];
                prevNode = me.snakeBody[index];
            }
            me.snakeTail = me.snakeBody[index];
            me.snakeTail.next = me.snakeHead;
            me.snakeHead.prev = me.snakeTail;
            
            document.getElementById('selectMode').style.display = "none";
            document.getElementById('selectJump').style.display = "none";
            document.getElementById('modeDisplay').style.display = "inline";
            document.getElementById('jumpDisplay').style.display = "inline";

            playingBoard.foodEaten();
        };

        /**
        * This method handles what happens when the snake dies.
        * @method handleDeath
        */
        me.handleDeath = function() {
            function recordScore () {
                var difficulty = document.getElementById("selectMode").options[document.getElementById("selectMode").selectedIndex].text
                switch (difficulty) {
                    case "Very Easy":
                        var veryEasyHighScore = localStorage.jsSnakeveryEasyHighScore;
                        if (veryEasyHighScore == undefined) localStorage.setItem('jsSnakeVeryEasyHighScore', me.snakeLength);
                        if (me.snakeLength > veryEasyHighScore) {
                            alert('Congratulations! You have beaten your previous high score in Very Easy, which was ' + veryEasyHighScore + '.');
                            localStorage.setItem('jsSnakeVeryEasyHighScore', me.snakeLength);
                        }
                        break;
                    case "Easy":
                        var easyHighScore = localStorage.jsSnakeEasyHighScore;
                        if (easyHighScore == undefined) localStorage.setItem('jsSnakeEasyHighScore', me.snakeLength);
                        if (me.snakeLength > easyHighScore) {
                            alert('Congratulations! You have beaten your previous high score in Easy, which was ' + easyHighScore + '.');
                            localStorage.setItem('jsSnakeEasyHighScore', me.snakeLength);
                        }
                        break;
                    case "Medium":
                        var mediumHighScore = localStorage.jsSnakeMediumHighScore;
                        if (mediumHighScore == undefined) localStorage.setItem('jsSnakeMediumHighScore', me.snakeLength);
                        if (me.snakeLength > mediumHighScore) {
                            alert('Congratulations! You have beaten your previous high score in Medium, which was ' + mediumHighScore + '.');
                            localStorage.setItem('jsSnakeMediumHighScore', me.snakeLength);
                        }
                        break;
                    case "Hard":
                        var hardHighScore = localStorage.jsSnakeHardHighScore;
                        if (hardHighScore == undefined) localStorage.setItem('jsSnakeHardHighScore', me.snakeLength);
                        if (me.snakeLength > hardHighScore) {
                            alert('Congratulations! You have beaten your previous high score in Hard, which was ' + hardHighScore + '.');
                            localStorage.setItem('jsSnakeHardHighScore', me.snakeLength);
                        }
                        break;
                    case "Very Hard":
                        var veryHardHighScore = localStorage.jsSnakeVeryHardHighScore;
                        if (veryHardHighScore == undefined) localStorage.setItem('jsSnakeVeryHardHighScore', me.snakeLength);
                        if (me.snakeLength > veryHardHighScore) {
                            alert('Congratulations! You have beaten your previous high score in Very Hard, which was ' + veryHardHighScore + '.');
                            localStorage.setItem('jsSnakeVeryHardHighScore', me.snakeLength);
                        }
                        break;
                    case "Impossible":
                        var impossibleHighScore = localStorage.jsSnakeImpossibleHighScore;
                        if (impossibleHighScore == undefined) localStorage.setItem('jsSnakeImpossibleHighScore', me.snakeLength);
                        if (me.snakeLength > impossibleHighScore) {
                            alert('Congratulations! You have beaten your previous high score in Impossible, which was ' + impossibleHighScore + '.');
                            localStorage.setItem('jsSnakeImpossibleHighScore', me.snakeLength);
                        }
                        break;
                }
            }
            recordScore();
            me.snakeHead.elm.style.zIndex = getNextHighestZIndex(me.snakeBody);
            me.snakeHead.elm.className = me.snakeHead.elm.className.replace(/\bsnake-snakebody-alive\b/,'')
            me.snakeHead.elm.className += " snake-snakebody-dead";

            isDead = true;
            playingBoard.handleDeath();
            moveQueue.length = 0;
        };

        /**
        * This method sets a flag that lets the snake be alive again.
        * @method rebirth
        */
        me.rebirth = function() {
            isDead = false;
        };

        /**
        * This method reset the snake so it is ready for a new game.
        * @method reset
        */
        me.reset = function() {
            if (isDead === false) {return;}
            console.log("resetting");
            //console.log(moveQueue);
            //console.log(me.lastMove);
            //console.log(currentDirection);

            var blocks = [],
                curNode = me.snakeHead.next,
                nextNode;
            while (curNode !== me.snakeHead) {
                nextNode = curNode.next;
                curNode.prev = null;
                curNode.next = null;
                blocks.push(curNode);
                curNode = nextNode;
            }
            var elemsToClear = document.getElementsByClassName("snake-snakebody-block");
            //console.log(elemsToClear);
            for (var i = elemsToClear.length - 1; i >= 0; --i) {
                elemsToClear[i].remove();
            }
            
            //me.snakeHead.next = me.snakeHead;
            //me.snakeHead.prev = me.snakeHead;
            //me.snakeTail = me.snakeHead;
            //me.snakeLength = 1;
            //me.moves = 0;
//
            //for (var ii = 0; ii < blocks.length; ii++) {
            //    blocks[ii].elm.style.left = "-1000px";
            //    blocks[ii].elm.style.top = "-1000px";
            //    blocks[ii].elm.className = me.snakeHead.elm.className.replace(/\bsnake-snakebody-dead\b/,'')
            //    blocks[ii].elm.className += " snake-snakebody-alive";
            //}
//
            //blockPool.concat(blocks);
            //me.snakeHead.elm.className = me.snakeHead.elm.className.replace(/\bsnake-snakebody-dead\b/,'')
            //me.snakeHead.elm.className += " snake-snakebody-alive";
            //me.snakeHead.row = config.startRow || 1;
            //me.snakeHead.col = config.startCol || 1;
            //me.snakeHead.xPos = me.snakeHead.row * playingBoard.getBlockWidth();
            //me.snakeHead.yPos = me.snakeHead.col * playingBoard.getBlockHeight();
            //me.snakeHead.elm.style.left = me.snakeHead.xPos + "px";
            //me.snakeHead.elm.style.top = me.snakeHead.yPos + "px";
            document.getElementById('selectMode').style.display = "inline";
            document.getElementById('modeDisplay').style.display = "none";
            document.getElementById('selectJump').style.display = "inline";
            if(document.getElementById('selectJump').value >= 0){
                document.getElementById('jumpDisplay').style.display = "none";
            }
            
            blockPool = [];
            
            me.snakeBody = {};
            me.snakeBody["b0"] = new SnakeBlock(); // create snake head
            me.snakeBody["b0"].row = config.startRow || 1;
            me.snakeBody["b0"].col = config.startCol || 1;
            me.snakeBody["b0"].xPos = me.snakeBody["b0"].row * playingBoard.getBlockWidth();
            me.snakeBody["b0"].yPos = me.snakeBody["b0"].col * playingBoard.getBlockHeight();
            me.snakeBody["b0"].elm = createSnakeElement();
            me.snakeBody["b0"].elmStyle = me.snakeBody["b0"].elm.style;
            playingBoard.getBoardContainer().appendChild( me.snakeBody["b0"].elm );
            me.snakeBody["b0"].elm.style.left = me.snakeBody["b0"].xPos + "px";
            me.snakeBody["b0"].elm.style.top = me.snakeBody["b0"].yPos + "px";
            me.snakeBody["b0"].next = me.snakeBody["b0"];
            me.snakeBody["b0"].prev = me.snakeBody["b0"];
            
            me.snakeLength = 1;
            me.moves = 0;
            me.snakeHead = me.snakeBody["b0"];
            me.snakeTail = me.snakeBody["b0"];
            me.snakeHead.elm.className = me.snakeHead.elm.className.replace(/\bsnake-snakebody-dead\b/,'');
            me.snakeHead.elm.className += " snake-snakebody-alive";
            
            createBlocks(growthIncr*2);
        };
        
        me.pressF = function(xOffset, yOffset) {
            console.log("F2");
            var blockXPos = [60, 60, 60, 60, 60, 60, 60, 80, 100, 120, 80, 100];
            var blockYPos = [60, 80, 100, 120, 140, 160, 180, 60, 60, 60, 120, 120];
            //if(!xOffset) {
            //    console.log("!xOffset");
            //    var xOffset = 0;
            //    var yOffset = 0;
            //}
            for(var i = 0; i < blockXPos.length; i++) {
                var testBlock = new SnakeBlock({row:3,col:3});
                testBlock.xPos = blockXPos[i]+xOffset;
                testBlock.yPos = blockYPos[i]+yOffset;
                testBlock.elm = createSnakeElement();
                testBlock.elmStyle = testBlock.elm.style;
                testBlock.elmStyle.left = testBlock.xPos + "px";
                testBlock.elmStyle.top = testBlock.yPos + "px";
                testBlock.elm.className += " snake-snakebody-alive"
                playingBoard.getBoardContainer().appendChild(testBlock.elm);
                blockPool[blockPool.length] = testBlock;
                playingBoard.grid[3][3] = 1;
            }
            xOffset += 140;
            //console.log(window.innerWidth-60);
            if (xOffset > window.innerWidth-120) {
                xOffset = 0;
                yOffset += 160;
            }
            console.log("xoffset"+xOffset);
            console.log("yoffset"+yOffset);
            return (xOffset*1000)+yOffset;
        };

        // ---------------------------------------------------------------------
        // Initialize
        // ---------------------------------------------------------------------
        createBlocks(growthIncr*2);
        xPosShift[0] = 0;
        xPosShift[1] = playingBoard.getBlockWidth();
        xPosShift[2] = 0;
        xPosShift[3] = -1 * playingBoard.getBlockWidth();

        yPosShift[0] = -1 * playingBoard.getBlockHeight();
        yPosShift[1] = 0;
        yPosShift[2] = playingBoard.getBlockHeight();
        yPosShift[3] = 0;
    };
})();

/**
* This class manages the food which the snake will eat.
* @class Food
* @constructor
* @namespace SNAKE
* @param {Object} config The configuration object for the class. Contains playingBoard (the SNAKE.Board that this food resides in).
*/

SNAKE.Food = SNAKE.Food || (function() {

    // -------------------------------------------------------------------------
    // Private static variables and methods
    // -------------------------------------------------------------------------

    var instanceNumber = 0;

    function getRandomPosition(x, y){
        return Math.floor(Math.random()*(y+1-x)) + x;
    }

    // -------------------------------------------------------------------------
    // Contructor + public and private definitions
    // -------------------------------------------------------------------------

    /*
        config options:
            playingBoard - the SnakeBoard that this object belongs too.
    */
    return function(config) {

        if (!config||!config.playingBoard) {return;}

        // ----- private variables -----

        var me = this;
        var playingBoard = config.playingBoard;
        var fRow, fColumn;
        var myId = instanceNumber++;

        var elmFood = document.createElement("div");
        elmFood.setAttribute("id", "snake-food-"+myId);
        elmFood.className = "snake-food-block";
        elmFood.style.width = playingBoard.getBlockWidth() + "px";
        elmFood.style.height = playingBoard.getBlockHeight() + "px";
        elmFood.style.left = "-1000px";
        elmFood.style.top = "-1000px";
        playingBoard.getBoardContainer().appendChild(elmFood);

        // ----- public methods -----

        /**
        * @method getFoodElement
        * @return {DOM Element} The div the represents the food.
        */
        me.getFoodElement = function() {
            return elmFood;
        };

        /**
        * Randomly places the food onto an available location on the playing board.
        * @method randomlyPlaceFood
        */
        me.randomlyPlaceFood = function() {
            // if there exist some food, clear its presence from the board
            if (playingBoard.grid[fRow] && playingBoard.grid[fRow][fColumn] === playingBoard.getGridFoodValue()){
                playingBoard.grid[fRow][fColumn] = 0;
            }

            var row = 0, col = 0, numTries = 0;

            var maxRows = playingBoard.grid.length-1;
            var maxCols = playingBoard.grid[0].length-1;

            while (playingBoard.grid[row][col] !== 0){
                row = getRandomPosition(1, maxRows);
                col = getRandomPosition(1, maxCols);

                // in some cases there may not be any room to put food anywhere
                // instead of freezing, exit out
                numTries++;
                if (numTries > 20000){
                    row = -1;
                    col = -1;
                    break;
                }
            }

            playingBoard.grid[row][col] = playingBoard.getGridFoodValue();
            fRow = row;
            fColumn = col;
            elmFood.style.top = row * playingBoard.getBlockHeight() + "px";
            elmFood.style.left = col * playingBoard.getBlockWidth() + "px";
        };
    };
})();

/**
* This class manages playing board for the game.
* @class Board
* @constructor
* @namespace SNAKE
* @param {Object} config The configuration object for the class. Set fullScreen equal to true if you want the game to take up the full screen, otherwise, set the top, left, width and height parameters.
*/

SNAKE.Board = SNAKE.Board || (function() {

    // -------------------------------------------------------------------------
    // Private static variables and methods
    // -------------------------------------------------------------------------

    var instanceNumber = 0;

    // this function is adapted from the example at http://greengeckodesign.com/blog/2007/07/get-highest-z-index-in-javascript.html
    function getNextHighestZIndex(myObj) {
        var highestIndex = 0,
            currentIndex = 0,
            ii;
        for (ii in myObj) {
            if (myObj[ii].elm.currentStyle){
                currentIndex = parseFloat(myObj[ii].elm.style["z-index"],10);
            }else if(window.getComputedStyle) {
                currentIndex = parseFloat(document.defaultView.getComputedStyle(myObj[ii].elm,null).getPropertyValue("z-index"),10);
            }
            if(!isNaN(currentIndex) && currentIndex > highestIndex){
                highestIndex = currentIndex;
            }
        }
        return(highestIndex+1);
    }

    /*
        This function returns the width of the available screen real estate that we have
    */
    function getClientWidth(){
        var myWidth = 0;
        if( typeof window.innerWidth === "number" ) {
            myWidth = window.innerWidth;//Non-IE
        } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
            myWidth = document.documentElement.clientWidth;//IE 6+ in 'standards compliant mode'
        } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
            myWidth = document.body.clientWidth;//IE 4 compatible
        }
        return myWidth;
    }
    /*
        This function returns the height of the available screen real estate that we have
    */
    function getClientHeight(){
        var myHeight = 0;
        if( typeof window.innerHeight === "number" ) {
            myHeight = window.innerHeight;//Non-IE
        } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
            myHeight = document.documentElement.clientHeight;//IE 6+ in 'standards compliant mode'
        } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
            myHeight = document.body.clientHeight;//IE 4 compatible
        }
        return myHeight;
    }

    // -------------------------------------------------------------------------
    // Contructor + public and private definitions
    // -------------------------------------------------------------------------

    return function(inputConfig) {

        // --- private variables ---
        var me = this,
            myId = instanceNumber++,
            config = inputConfig || {},
            blockWidth = 20,
            blockHeight = 20,
            GRID_FOOD_VALUE = -1, // the value of a spot on the board that represents snake food, MUST BE NEGATIVE
            myFood,
            mySnake,
            boardState = 1, // 0: in active; 1: awaiting game start; 2: playing game
            myKeyListener,
            isPaused = false,//note: both the board and the snake can be paused
            // Board components
            elmContainer, elmPlayingField, elmAboutPanel, elmLengthPanel, elmMovePanel, elmWelcome, elmTryAgain, elmPauseScreen;

        // --- public variables ---
        me.grid = [];

        // ---------------------------------------------------------------------
        // private functions
        // ---------------------------------------------------------------------

        function createBoardElements() {
            elmPlayingField = document.createElement("div");
            elmPlayingField.setAttribute("id", "playingField");
            elmPlayingField.className = "snake-playing-field";

            SNAKE.addEventListener(elmPlayingField, "click", function() {
                elmContainer.focus();
            }, false);

            elmPauseScreen = document.createElement("div");
            elmPauseScreen.className = "snake-pause-screen";
            elmPauseScreen.innerHTML = "<div style='padding:10px;'>[Paused]<p/>Press [space] to unpause.</div>";

            elmAboutPanel = document.createElement("div");
            elmAboutPanel.className = "snake-panel-component";
            elmAboutPanel.innerHTML = "<a href='https://gitlab.com/minecirecire/javascript-snake-fork' class='snake-link'>source code</a> - <a href='https://github.com/patorjk/JavaScript-Snake' class='snake-link'>Forked from JavaScript Snake by patorjk</a> and <a href='https://gitlab.com/david.sharick/javascript-snake-fork' class='snake-link'>David Sharick</a>";

            elmLengthPanel = document.createElement("div");
            elmLengthPanel.className = "snake-panel-component";
            elmLengthPanel.innerHTML = "Length: 1";
            
            elmMovePanel = document.createElement("div");
            elmMovePanel.className = "snake-panel-component";
            elmMovePanel.innerHTML = "Moves: 0";

            elmWelcome = createWelcomeElement();
            elmTryAgain = createTryAgainElement();

            SNAKE.addEventListener( elmContainer, "keyup", function(evt) {
                if (!evt) var evt = window.event;
                evt.cancelBubble = true;
                if (evt.stopPropagation) {evt.stopPropagation();}
                if (evt.preventDefault) {evt.preventDefault();}
                return false;
            }, false);

            elmContainer.className = "snake-game-container";

            elmPauseScreen.style.zIndex = 10000;
            elmContainer.appendChild(elmPauseScreen);
            elmContainer.appendChild(elmPlayingField);
            elmContainer.appendChild(elmAboutPanel);
            elmContainer.appendChild(elmLengthPanel);
            elmContainer.appendChild(elmMovePanel);
            elmContainer.appendChild(elmWelcome);
            elmContainer.appendChild(elmTryAgain);

            mySnake = new SNAKE.Snake({playingBoard:me,startRow:2,startCol:2});
            myFood = new SNAKE.Food({playingBoard: me});

            elmWelcome.style.zIndex = 1000;
        }
        function maxBoardWidth() {
            return MAX_BOARD_COLS * me.getBlockWidth();
        }
        function maxBoardHeight() {
            return MAX_BOARD_ROWS * me.getBlockHeight();
        }

        function createWelcomeElement() {
             var tmpElm = document.createElement("div");
            tmpElm.id = "sbWelcome" + myId;
            tmpElm.className = "snake-welcome-dialog";

            var welcomeTxt = document.createElement("div");
            var fullScreenText = "";
            if (config.fullScreen) {
                fullScreenText = "On Windows, press F11 to play in Full Screen mode.";
            }
            welcomeTxt.innerHTML = "JavaScript Snake<p></p>Use the <strong>arrow keys</strong> on your keyboard to play the game. " + fullScreenText + "<p></p>";
            var welcomeStart = document.createElement("button");
            welcomeStart.appendChild(document.createTextNode("Play Game"));
            var loadGame = function() {
                SNAKE.removeEventListener(window, "keyup", kbShortcut, false);
                tmpElm.style.display = "none";
                me.setBoardState(1);
                me.getBoardContainer().focus();
            };

            var kbShortcut = function(evt) {
                if (!evt) var evt = window.event;
                var keyNum = (evt.which) ? evt.which : evt.keyCode;
                if (keyNum === 32 || keyNum === 13) {
                    loadGame();
                }
            };
            SNAKE.addEventListener(window, "keyup", kbShortcut, false);
            SNAKE.addEventListener(welcomeStart, "click", loadGame, false);

            tmpElm.appendChild(welcomeTxt);
            tmpElm.appendChild(welcomeStart);
            return tmpElm;
        }

        function createTryAgainElement() {
            var tmpElm = document.createElement("div");
            tmpElm.id = "sbTryAgain" + myId;
            tmpElm.className = "snake-try-again-dialog";

            var tryAgainTxt = document.createElement("div");
            tryAgainTxt.innerHTML = "JavaScript Snake<p></p>You died :(<p></p>";
            var tryAgainStart = document.createElement("button");
            tryAgainStart.appendChild( document.createTextNode("Play Again?"));

            var reloadGame = function() {
                tmpElm.style.display = "none";
                me.resetBoard();
                me.setBoardState(1);
                me.getBoardContainer().focus();
                offsetsCombined = 0;
            };
            
            var offsetsCombined = 0;

            var kbTryAgainShortcut = function(evt) {
                if (boardState !== 0 || tmpElm.style.display !== "block") {return;}
                if (!evt) var evt = window.event;
                var keyNum = (evt.which) ? evt.which : evt.keyCode;
                if (keyNum === 32 || keyNum === 13) {
                    reloadGame();
                }
                if (keyNum === 70) {
                    console.log("Pressed F");
                    //console.log(SnakeBlock);
                    offsetsCombined = mySnake.pressF(Math.round(offsetsCombined/1000), offsetsCombined%1000);
                    console.log(offsetsCombined);
                }
            };
            SNAKE.addEventListener(window, "keyup", kbTryAgainShortcut, true);
            
            SNAKE.addEventListener(tryAgainStart, "click", reloadGame, false);
            tmpElm.appendChild(tryAgainTxt);
            tmpElm.appendChild(tryAgainStart);
            return tmpElm;
        }
        // ---------------------------------------------------------------------
        // public functions
        // ---------------------------------------------------------------------

        me.setPaused = function(val) {
            isPaused = val;
            mySnake.setPaused(val);
            if (isPaused) {
                elmPauseScreen.style.display = "block";
            } else {
                elmPauseScreen.style.display = "none";
            }
        };
        me.getPaused = function() {
            return isPaused;
        };

        /**
        * Resets the playing board for a new game.
        * @method resetBoard
        */
        me.resetBoard = function() {
            SNAKE.removeEventListener(elmContainer, "keydown", myKeyListener, false);
            mySnake.reset();
            elmLengthPanel.innerHTML = "Length: 1";
            elmMovePanel.innerHTML = "Moves: 0";
            me.setupPlayingField();
        };
        /**
        * Gets the current state of the playing board. There are 3 states: 0 - Welcome or Try Again dialog is present. 1 - User has pressed "Start Game" on the Welcome or Try Again dialog but has not pressed an arrow key to move the snake. 2 - The game is in progress and the snake is moving.
        * @method getBoardState
        * @return {Number} The state of the board.
        */
        me.getBoardState = function() {
            return boardState;
        };
        /**
        * Sets the current state of the playing board. There are 3 states: 0 - Welcome or Try Again dialog is present. 1 - User has pressed "Start Game" on the Welcome or Try Again dialog but has not pressed an arrow key to move the snake. 2 - The game is in progress and the snake is moving.
        * @method setBoardState
        * @param {Number} state The state of the board.
        */
        me.setBoardState = function(state) {
            boardState = state;
        };
        /**
        * @method getGridFoodValue
        * @return {Number} A number that represents food on a number representation of the playing board.
        */
        me.getGridFoodValue = function() {
            return GRID_FOOD_VALUE;
        };
        /**
        * @method getPlayingFieldElement
        * @return {DOM Element} The div representing the playing field (this is where the snake can move).
        */
        me.getPlayingFieldElement = function() {
            return elmPlayingField;
        };
        /**
        * @method setBoardContainer
        * @param {DOM Element or String} myContainer Sets the container element for the game.
        */
        me.setBoardContainer = function(myContainer) {
            if (typeof myContainer === "string") {
                myContainer = document.getElementById(myContainer);
            }
            if (myContainer === elmContainer) {return;}
            elmContainer = myContainer;
            elmPlayingField = null;

            me.setupPlayingField();
        };
        /**
        * @method getBoardContainer
        * @return {DOM Element}
        */
        me.getBoardContainer = function() {
            return elmContainer;
        };
        /**
        * @method getBlockWidth
        * @return {Number}
        */
        me.getBlockWidth = function() {
            return blockWidth;
        };
        /**
        * @method getBlockHeight
        * @return {Number}
        */
        me.getBlockHeight = function() {
            return blockHeight;
        };
        /**
        * Sets up the playing field.
        * @method setupPlayingField
        */
        me.setupPlayingField = function () {

            if (!elmPlayingField) {createBoardElements();} // create playing field

            // calculate width of our game container
            var cWidth, cHeight, cTop, cLeft;
            if (config.fullScreen === true) {
                cTop = 0;
                cLeft = 0;
                cWidth = getClientWidth()-20;
                cHeight = getClientHeight()-20;
                
            } else {
                cTop = config.top;
                cLeft = config.left;
                cWidth = config.width;
                cHeight = config.height;
            }

            // define the dimensions of the board and playing field
            var wEdgeSpace = me.getBlockWidth()*2 + (cWidth % me.getBlockWidth());
            var fWidth = Math.min(maxBoardWidth()-wEdgeSpace,cWidth-wEdgeSpace);
            var hEdgeSpace = me.getBlockHeight()*3 + (cHeight % me.getBlockHeight());
            var fHeight = Math.min(maxBoardHeight()-hEdgeSpace,cHeight-hEdgeSpace);

            elmContainer.style.left = cLeft + "px";
            elmContainer.style.top = cTop + "px";
            elmContainer.style.width = cWidth + "px";
            elmContainer.style.height = cHeight + "px";
            elmPlayingField.style.left = me.getBlockWidth() + "px";
            elmPlayingField.style.top  = me.getBlockHeight() + "px";
            elmPlayingField.style.width = fWidth + "px";
            elmPlayingField.style.height = fHeight + "px";

            // the math for this will need to change depending on font size, padding, etc
            // assuming height of 14 (font size) + 8 (padding)
            var bottomPanelHeight = hEdgeSpace - me.getBlockHeight();
            var pLabelTop = me.getBlockHeight() + fHeight + Math.round((bottomPanelHeight - 30)/2) + "px";

            elmAboutPanel.style.top = pLabelTop;
            elmAboutPanel.style.width = "450px";
            elmAboutPanel.style.left = Math.round(cWidth/2) - Math.round(450/2) + "px";

            elmLengthPanel.style.top = pLabelTop;
            elmLengthPanel.style.left = cWidth - 120 + "px";

            elmMovePanel.style.top = pLabelTop;
            elmMovePanel.style.right = cWidth - 120 + "px";

            // if width is too narrow, hide the about panel
            if (cWidth < 700) {
                elmAboutPanel.style.display = "none";
            } else {
                elmAboutPanel.style.display = "block";
            }

            me.grid = [];
            var numBoardCols = fWidth / me.getBlockWidth() + 2;
            var numBoardRows = fHeight / me.getBlockHeight() + 2;

            for (var row = 0; row < numBoardRows; row++) {
                me.grid[row] = [];
                for (var col = 0; col < numBoardCols; col++) {
                    if (col === 0 || row === 0 || col === (numBoardCols-1) || row === (numBoardRows-1)) {
                        me.grid[row][col] = 1; // an edge
                    } else {
                        me.grid[row][col] = 0; // empty space
                    }
                }
            }

            myFood.randomlyPlaceFood();

            myKeyListener = function(evt) {
                if (!evt) var evt = window.event;
                var keyNum = (evt.which) ? evt.which : evt.keyCode;

                if (me.getBoardState() === 1) {
                    if ( !(keyNum >= 37 && keyNum <= 40) && !(keyNum === 87 || keyNum === 65 || keyNum === 83 || keyNum === 68)) {return;} // if not an arrow key, leave

                    // This removes the listener added at the #listenerX line
                    SNAKE.removeEventListener(elmContainer, "keydown", myKeyListener, false);

                    myKeyListener = function(evt) {
                        if (!evt) var evt = window.event;
                        var keyNum = (evt.which) ? evt.which : evt.keyCode;

                        console.log(keyNum);
                        if (keyNum === 32) {
							if(me.getBoardState()!=0)
                                me.setPaused(!me.getPaused());
                        }

                        mySnake.handleArrowKeys(keyNum);

                        evt.cancelBubble = true;
                        if (evt.stopPropagation) {evt.stopPropagation();}
                        if (evt.preventDefault) {evt.preventDefault();}
                        return false;
                    };
                    SNAKE.addEventListener( elmContainer, "keydown", myKeyListener, false);

                    mySnake.rebirth();
                    mySnake.handleArrowKeys(keyNum);
                    me.setBoardState(2); // start the game!
                    mySnake.go();
                }

                evt.cancelBubble = true;
                if (evt.stopPropagation) {evt.stopPropagation();}
                if (evt.preventDefault) {evt.preventDefault();}
                return false;
            };

            // Search for #listenerX to see where this is removed
            SNAKE.addEventListener( elmContainer, "keydown", myKeyListener, false);
        };

        /**
        * This method is called when the snake has eaten some food.
        * @method foodEaten
        */
        me.foodEaten = function() {
            elmLengthPanel.innerHTML = "Length: " + mySnake.snakeLength;
            myFood.randomlyPlaceFood();
        };
        
        /**
        * This method is called when the snake makes a move.
        * @method moveMade
        */
        me.moveMade = function() {
            elmMovePanel.innerHTML = "Moves: " + mySnake.moves;
        };

        /**
        * This method is called when the snake dies.
        * @method handleDeath
        */
        me.handleDeath = function() {
            var index = Math.max(getNextHighestZIndex( mySnake.snakeBody), getNextHighestZIndex( {tmp:{elm:myFood.getFoodElement()}} ));
            elmContainer.removeChild(elmTryAgain);
            elmContainer.appendChild(elmTryAgain);
            elmTryAgain.style.zIndex = index;
            elmTryAgain.style.display = "block";
            me.setBoardState(0);
        };

        // ---------------------------------------------------------------------
        // Initialize
        // ---------------------------------------------------------------------

        config.fullScreen = (typeof config.fullScreen === "undefined") ? false : config.fullScreen;
        config.top = (typeof config.top === "undefined") ? 0 : config.top;
        config.left = (typeof config.left === "undefined") ? 0 : config.left;
        config.width = (typeof config.width === "undefined") ? 400 : config.width;
        config.height = (typeof config.height === "undefined") ? 400 : config.height;

        if (config.fullScreen) {
            SNAKE.addEventListener(window,"resize", function() {
                me.setupPlayingField();
            }, false);
        }

        me.setBoardState(0);

        if (config.boardContainer) {
            me.setBoardContainer(config.boardContainer);
        }

    }; // end return function
})();
function getHighScore () {
    document.getElementById('high-score').addEventListener('click', function () {
        var difficulty = document.getElementById("selectMode").options[document.getElementById("selectMode").selectedIndex].text
        switch (difficulty) {
            case "Very Easy":
                if (localStorage.jsSnakeVeryEasyHighScore == undefined) alert('You have not played in this difficulty yet!');
                else alert('Your current high score in Very Easy is ' + localStorage.jsSnakeVeryEasyHighScore + '.');
                break;
            case "Easy":
                if (localStorage.jsSnakeEasyHighScore == undefined) alert('You have not played in this difficulty yet!');
                else alert('Your current high score in Easy is ' + localStorage.jsSnakeEasyHighScore + '.');
                break;
            case "Medium":
                if (localStorage.jsSnakeMediumHighScore == undefined) alert('You have not played in this difficulty yet!');
                else alert('Your current high score in Medium is ' + localStorage.jsSnakeMediumHighScore + '.');
                break;
            case "Hard":
                if (localStorage.jsSnakeHardHighScore == undefined) alert('You have not played in this difficulty yet!');
                else alert('Your current high score in Hard is ' + localStorage.jsSnakeHardHighScore + '.');
                break;
            case "Very Hard":
                if (localStorage.jsSnakeVeryHardHighScore == undefined) alert('You have not played in this difficulty yet!');
                else alert('Your current high score in Very Hard is ' + localStorage.jsSnakeVeryHardHighScore + '.');
                break;
            case "Impossible":
                if (localStorage.jsSnakeImpossibleHighScore == undefined) alert('You have not played in this difficulty yet!');
                else alert('Your current high score in Impossible is ' + localStorage.jsSnakeImpossibleHighScore + '.');
                break;
        }
    });
}
getHighScore();

function getHighScoreTotal () {
    document.getElementById('high-score-total').addEventListener('click', function () {
        if ( localStorage.jsSnakeVeryEasyHighScore != undefined && localStorage.jsSnakeEasyHighScore != undefined && localStorage.jsSnakeMediumHighScore != undefined && localStorage.jsSnakeHardHighScore != undefined && localStorage.jsSnakeVeryHardHighScore != undefined && localStorage.jsSnakeImpossibleHighScore != undefined ) {
            alert('Your current overall high score is '+ Math.max(localStorage.jsSnakeVeryEasyHighScore, localStorage.jsSnakeEasyHighScore, localStorage.jsSnakeMediumHighScore, localStorage.jsSnakeHardHighScore, localStorage.jsSnakeVeryHardHighScore, localStorage.jsSnakeImpossibleHighScore) + '.'); //add difficulty specification later
        } else alert('Play all difficulties before using this feature.');
    });
}
getHighScoreTotal();

//40, 13.3333
//30, 18
//32, 8.4
//-2, 25.6

//for(var b in window) {
//    if(window.hasOwnProperty(b)) console.log(b);
//}
//console.log(window);